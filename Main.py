import pandas as pd
from Iris import *
from OneClassSVM import *
from IsolationForest import *
from LocalOutlierFactor import *
from Covariance import *

# load iris data
fullIrisData = pd.read_csv("iriscsv.csv")

# make class of each type
Setosa = Iris(fullIrisData.loc[fullIrisData.variety == 'Setosa'], "Setosa")
Versicolor = Iris(fullIrisData.loc[fullIrisData.variety == 'Versicolor'], "Versicolor")
Virginica = Iris(fullIrisData.loc[fullIrisData.variety == 'Virginica'], "Virginica")

# Go through each class and get results for One Class SVM
getResultsOneClassSVM(Setosa, Versicolor, Virginica)
getResultsOneClassSVM(Versicolor, Virginica, Setosa)
getResultsOneClassSVM(Virginica, Setosa, Versicolor)

# Go through each class and get results for Isolation Forest
getResultsIsolationForest(Setosa, Versicolor, Virginica)
getResultsIsolationForest(Versicolor, Virginica, Setosa)
getResultsIsolationForest(Virginica, Setosa, Versicolor)

# Go through each class and get results for Local Outlier Factor
getResultsLocalOutlierFactor(Setosa, Versicolor, Virginica)
getResultsLocalOutlierFactor(Versicolor, Virginica, Setosa)
getResultsLocalOutlierFactor(Virginica, Setosa, Versicolor)

# Go through each class and get results for Covariance
getResultsCovariance(Setosa, Versicolor, Virginica)
getResultsCovariance(Versicolor, Virginica, Setosa)
getResultsCovariance(Virginica, Setosa, Versicolor)
