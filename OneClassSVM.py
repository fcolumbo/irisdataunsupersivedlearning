## One-class SVM with non-linear kernel (RBF)
## https://scikit-learn.org/stable/auto_examples/svm/plot_oneclass.html#sphx-glr-auto-examples-svm-plot-oneclass-py
## https://scikit-learn.org/stable/auto_examples/miscellaneous
# /plot_anomaly_comparison.html#sphx-glr-auto-examples-miscellaneous-plot-anomaly-comparison-py
from sklearn import svm
from GetResults import *


def getResultsOneClassSVM(trainingObject, Outlier1Object, Outlier2Object):
    clf = svm.OneClassSVM(nu=0.1, kernel="rbf", gamma=0.1)
    getResults(clf, trainingObject, Outlier1Object, Outlier2Object, 'One Class SVM for ')
