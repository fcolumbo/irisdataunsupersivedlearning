def getResults(clf, trainingObject, Outlier1Object, Outlier2Object, message):
    clf.fit(trainingObject.trainingData)
    print(message, trainingObject.name)
    print(clf.predict(trainingObject.testingData), 'results of unseen data from the trained class',
          trainingObject.name)
    print(clf.predict(Outlier1Object.testingData), 'results from first non class testing data',
          Outlier1Object.name)
    print(clf.predict(Outlier2Object.testingData), 'results from second non class testing data',
          Outlier2Object.name)
