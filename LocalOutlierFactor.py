## Outlier detection with Local Outlier Factor (LOF)
## https://scikit-learn.org/stable/auto_examples/neighbors
# /plot_lof_outlier_detection.html#sphx-glr-auto-examples-neighbors-plot-lof-outlier-detection-py
from sklearn.neighbors import LocalOutlierFactor
from GetResults import *


def getResultsLocalOutlierFactor(trainingObject, Outlier1Object, Outlier2Object):
    clf = LocalOutlierFactor(novelty=True) #n_neighbors=10, contamination=0.1,
    getResults(clf, trainingObject, Outlier1Object, Outlier2Object, 'Local Outlier Factor for ')

