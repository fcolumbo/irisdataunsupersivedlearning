# Iris Data - Unsupervised learning - Novelty Detection
This repository has been created to hold expremental coding for unsupervised learning using the Iris data for novelity and outlier detection.

The aim of this is program is to identify if the new unseen data is or is not the same as the training data.

For example. 80% of the Setosa data is used for training. Then the the remaining 20% of the Setosa and data from Versicolor and Virginica is used to test.

If the model believes something is part of the same distribution / class the output is 1.

If the model believes something is not part of the same distribution / class the out put is -1.

This is my first attempt at unsupervised learning Novelty and Outlier detection and I have relied heavily on https://scikit-learn.org/stable/modules/outlier_detection.html for guidence.

In this case I am using Novelty Detection because the training data is not polluted with outliers.

I have not concerned myself (yet) with producing nice looking graphs instead the results are print arrays.

The ideal result for prefect prediction would be :

[ 1  1  1  1  1  1  1  1  1  1]

[-1 -1 -1 -1 -1 -1 -1 -1 -1 -1]

[-1 -1 -1 -1 -1 -1 -1 -1 -1 -1]

The actual results are as follows:

One Class SVM for  Setosa

[ 1 -1  1  1  1  1  1  1  1  1] results of unseen data from the trained class Setosa

[-1 -1 -1 -1 -1 -1 -1 -1 -1 -1] results from first non class testing data Versicolor

[-1 -1 -1 -1 -1 -1 -1 -1 -1 -1] results from second non class testing data Virginica

One Class SVM for  Versicolor

[ 1  1  1  1  1  1  1  1 -1  1] results of unseen data from the trained class Versicolor

[-1 -1 -1 -1 -1 -1 -1 -1 -1 -1] results from first non class testing data Virginica

[-1 -1 -1 -1 -1 -1 -1 -1 -1 -1] results from second non class testing data Setosa

One Class SVM for  Virginica

[1 1 1 1 1 1 1 1 1 1] results of unseen data from the trained class Virginica

[-1 -1 -1 -1 -1 -1 -1 -1 -1 -1] results from first non class testing data Setosa

[-1 -1 -1 -1 -1 -1 -1 -1 -1 -1] results from second non class testing data Versicolor

Isolation Forest for  Setosa

[ 1 -1  1 -1 -1  1  1  1  1  1] results of unseen data from the trained class Setosa

[-1 -1 -1 -1 -1 -1 -1 -1 -1 -1] results from first non class testing data Versicolor

[-1 -1 -1 -1 -1 -1 -1 -1 -1 -1] results from second non class testing data Virginica

Isolation Forest for  Versicolor

[ 1  1  1 -1  1  1  1  1 -1  1] results of unseen data from the trained class Versicolor

[-1 -1 -1 -1 -1 -1 -1 -1 -1 -1] results from first non class testing data Virginica

[-1 -1 -1 -1 -1 -1 -1 -1 -1 -1] results from second non class testing data Setosa

Isolation Forest for  Virginica

[ 1  1  1  1 -1  1  1  1 -1  1] results of unseen data from the trained class Virginica

[-1 -1 -1 -1 -1 -1 -1 -1 -1 -1] results from first non class testing data Setosa

[-1 -1 -1 -1 -1 -1 -1 -1 -1 -1] results from second non class testing data Versicolor

Local Outlier Factor for  Setosa

[ 1 -1  1  1  1  1  1  1  1  1] results of unseen data from the trained class Setosa

[-1 -1 -1 -1 -1 -1 -1 -1 -1 -1] results from first non class testing data Versicolor

[-1 -1 -1 -1 -1 -1 -1 -1 -1 -1] results from second non class testing data Virginica

Local Outlier Factor for  Versicolor

[1 1 1 1 1 1 1 1 1 1] results of unseen data from the trained class Versicolor

[-1  1  1 -1 -1  1  1  1  1  1] results from first non class testing data Virginica

[-1 -1 -1 -1 -1 -1 -1 -1 -1 -1] results from second non class testing data Setosa

Local Outlier Factor for  Virginica

[1 1 1 1 1 1 1 1 1 1] results of unseen data from the trained class Virginica

[-1 -1 -1 -1 -1 -1 -1 -1 -1 -1] results from first non class testing data Setosa

[ 1  1 -1 -1  1  1  1  1 -1  1] results from second non class testing data Versicolor

Covariance for  Setosa

[ 1 -1  1 -1  1  1  1  1  1  1] results of unseen data from the trained class Setosa

[-1 -1 -1 -1 -1 -1 -1 -1 -1 -1] results from first non class testing data Versicolor

[-1 -1 -1 -1 -1 -1 -1 -1 -1 -1] results from second non class testing data Virginica

Covariance for  Versicolor

[-1  1  1  1  1  1  1  1 -1  1] results of unseen data from the trained class Versicolor

[-1 -1 -1 -1 -1 -1 -1 -1 -1 -1] results from first non class testing data Virginica

[-1 -1 -1 -1 -1 -1 -1 -1 -1 -1] results from second non class testing data Setosa

Covariance for  Virginica

[ 1 -1  1  1  1  1  1  1  1  1] results of unseen data from the trained class Virginica

[-1 -1 -1 -1 -1 -1 -1 -1 -1 -1] results from first non class testing data Setosa

[-1 -1 -1 -1 -1 -1 -1 -1 -1 -1] results from second non class testing data Versicolor