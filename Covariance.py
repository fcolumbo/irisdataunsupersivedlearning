##
from sklearn.covariance import EllipticEnvelope
from GetResults import *


def getResultsCovariance(trainingObject, Outlier1Object, Outlier2Object):
    clf = EllipticEnvelope().fit(trainingObject.trainingData)#random_state=0
    getResults(clf, trainingObject, Outlier1Object, Outlier2Object, 'Covariance for ')
