## IsolationForest
## https://scikit-learn.org/stable/auto_examples/ensemble
# /plot_isolation_forest.html#sphx-glr-auto-examples-ensemble-plot-isolation-forest-py
from sklearn.ensemble import IsolationForest
from GetResults import *


def getResultsIsolationForest(trainingObject, Outlier1Object, Outlier2Object):
    clf = IsolationForest()
    getResults(clf, trainingObject, Outlier1Object, Outlier2Object, 'Isolation Forest for ')

